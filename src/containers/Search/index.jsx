import React from "react";
import SearchBox from "../../components/SearchBox";
import Results from "../../components/Results";

function Search() {
  const [issues, setIssues] = React.useState([]);
  const [searchText, setSearch] = React.useState("");

  //Improve performance waiting until user stops typing
  React.useEffect(() => {
    if (!searchText) return;
    const timeOut = setTimeout(async () => {
      try {
        const query = searchText + " repo:facebook/react&per_page=5";
        const response = await fetch(
          `https://api.github.com/search/issues?q=${query}`
        );
        const data = await response.json();
        /* const response = await axios.get(
          "https://api.github.com/search/issues?q=" + query
        ); */
        setIssues(data.items);
      } catch (error) {
        console.error(error);
      }
    }, 1000);
    return () => clearTimeout(timeOut);
  }, [searchText]);

  return (
    <>
      <SearchBox
        type="text"
        value={searchText}
        onChange={(e) => setSearch(e.target.value)}
        placeHolder="Search"
      />
      <Results items={issues} />
    </>
  );
}

export default Search;
