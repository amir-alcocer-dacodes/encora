import React from "react";
import "./styles.css";
export default function ResultItem({ title, ...rest }) {
  return (
    <li className="search-item" {...rest}>
      {title}
    </li>
  );
}
