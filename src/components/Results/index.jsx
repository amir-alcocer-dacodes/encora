import React from "react";
import ResultItem from "../ResultItem";
import "./styles.css";

export default function Results({ items = [] }) {
  return (
    <ul className="search-list">
      {items.map(({ title }, index) => (
        <ResultItem title={title} key={index + "-item"} />
      ))}
    </ul>
  );
}
