import React from "react";
import "./styles.css";
export default function SearchBox(props) {
  return <input className="search-box" {...props} />;
}
