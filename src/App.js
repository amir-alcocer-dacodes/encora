import Search from "./containers/Search";
import './App.css';

function App() {
  return (
    <div className="container center">
      <div className="half-screen">
        <Search />
      </div>
    </div>
  );
}

export default App;
